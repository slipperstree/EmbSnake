/*-----------------------------------------------------------------------
|                            FILE DESCRIPTION                           |
-----------------------------------------------------------------------*/
/*----------------------------------------------------------------------
  - File name     : init.c
  - Author        : zeweni
  - Update date   : 2020.01.11
  -	Copyright(C)  : 2020-2021 zeweni. All rights reserved.
-----------------------------------------------------------------------*/
/*-----------------------------------------------------------------------
|                               INCLUDES                                |
-----------------------------------------------------------------------*/
#include "main.h"
#include "common.h"
/*-----------------------------------------------------------------------
|                                 DATA                                  |
-----------------------------------------------------------------------*/
#define PWM0C 		(*(unsigned int volatile xdata *)0xFF00)
#define PWM0CKS 	(*(unsigned int volatile xdata *)0xFF02)
#define PWM00T1 	(*(unsigned int volatile xdata *)0xFF10)
#define PWM00T2 	(*(unsigned int volatile xdata *)0xFF12)
#define PWM00CR 	(*(unsigned int volatile xdata *)0xFF14)

#define PWM04T1 	(*(unsigned int volatile xdata *)0xFF30)
#define PWM04T2 	(*(unsigned int volatile xdata *)0xFF32)
#define PWM04CR 	(*(unsigned int volatile xdata *)0xFF34)

/*--------------------------------------------------------
| @Description: MCU peripheral initialization function   |
--------------------------------------------------------*/

static void STC8x_SYSCLK_Config(void);
static void STC8x_UART_Config(void);
static void STC8x_GPIO_Config(void);
static void STC8x_TIMER_Config(void);
static void STC8x_SPI_Config(void);
static void STC8x_ADC_Config(void);
static void STC8x_PWM_Config(void);

/*-----------------------------------------------------------------------
|                               FUNCTION                                |
-----------------------------------------------------------------------*/

/**
  * @name    STC8x_System_Init
  * @brief   MCU initialization function
  * @param   None
  * @return  None
***/
void STC8x_System_Init(void)
{
	DELAY_POS(); /* Power on stability delay */	
	STC8x_SYSCLK_Config(); /* Initialize system clock */
	delay_init();

	STC8x_GPIO_Config();
	STC8x_UART_Config();
	STC8x_TIMER_Config();
	//STC8x_SPI_Config();  // SPI用于学电板载的FLASH芯片而不是LCD(暂时没有用到)
	STC8x_ADC_Config();
	STC8x_PWM_Config();
	EEPROM_Init(ENABLE);
	
	NVIC_GLOBAL_ENABLE();
}

/**
  * @name    STC8x_SYSCLK_Config
  * @brief   MCU SYSCLK initialization function
  * @param   None
  * @return  None
***/
static void STC8x_SYSCLK_Config(void)
{
	SYSCLK_InitType SYSCLK_InitStruct={0}; /* Declare structure */

	SYSCLK_InitStruct.MCLKSrc = AUTO;
	SYSCLK_InitStruct.SCLKDiv = 0; /* if SCLKDiv = 0, Not output */
	SYSCLK_InitStruct.SCLKOutPin = SCLK_OUT_P16;
	SYSCLK_Init(&SYSCLK_InitStruct);
}

/**
  * @name    STC8x_GPIO_Config
  * @brief   MCU GPIO initialization function
  * @param   None
  * @return  None
***/
static void STC8x_GPIO_Config(void)
{
	// 全部设置为双向口模式
	GPIO_MODE_WEAK_PULL(GPIO_P0,Pin_All);
	GPIO_MODE_WEAK_PULL(GPIO_P1,Pin_All);
	GPIO_MODE_WEAK_PULL(GPIO_P2,Pin_All);
	GPIO_MODE_WEAK_PULL(GPIO_P3,Pin_All);
	GPIO_MODE_WEAK_PULL(GPIO_P4,Pin_All);
	GPIO_MODE_WEAK_PULL(GPIO_P5,Pin_All);

	// P1.7设置为ADC
	GPIO_MODE_IN_FLOATING(GPIO_P1,Pin_7);
}

/**
  * @name    STC8x_TIMER_Config
  * @brief   MCU TIMER initialization function
  * @param   None
  * @return  None
***/
static void STC8x_TIMER_Config(void)
{
	TIMER_InitType TIMER_InitStruct={0};
	
	TIMER_InitStruct.Mode = TIMER_16BitAutoReload;
	TIMER_InitStruct.Time = 1000;     //1ms
	TIMER_InitStruct.Run = ENABLE;
	TIMER0_Init(&TIMER_InitStruct);
	NVIC_TIMER0_Init(NVIC_PR0,ENABLE);
}

/**
  * @name    STC8x_UART_Config
  * @brief   MCU UART initialization function
  * @param   None
  * @return  None
***/
static void STC8x_UART_Config(void)
{
	UART_InitType UART_InitStruct = {0};
	UART_BRTGen_Type UART_BRTGenStruct;
	
	/* UART1 TXD */
	GPIO_MODE_OUT_PP(GPIO_P3,Pin_1);
	/* UART1 RXD */
	GPIO_MODE_IN_FLOATING(GPIO_P3,Pin_0); 

	UART_InitStruct.Mode = UART_8bit_BRTx;
	UART_InitStruct.BRTGen = UART_BRT_TIM2;
	UART_InitStruct.BRTMode = UART_BRT_1T;
	UART_InitStruct.BaudRate = 115200;
	UART_InitStruct.RxEnable = ENABLE;
	UART1_Init(&UART_InitStruct);
	NVIC_UART1_Init(NVIC_PR3,ENABLE);
}

/**
  * @name    STC8x_ADC_Config
  * @brief   MCU ADC initialization function
  * @param   None
  * @return  None
***/
static void STC8x_ADC_Config(void)
{
	ADC_InitType ADC_InitStruct = {0};

	// 这个AD是悬空的用来产生随机数
	ADC_InitStruct.Power = ENABLE; // ADC power control bit
	ADC_InitStruct.Channel = ADC_Channel_P04_8G2K64S2_S4; // ADC channel selection
	ADC_InitStruct.Speed = 0X0F; // The maximum ADC conversion speed (working clock frequency) is 0x0f
	ADC_InitStruct.Align = ADC_Right; // ADC data format alignment
	ADC_InitStruct.Run = ENABLE; //  ADC conversion operation control bit
	ADC_Init(&ADC_InitStruct);
	//NVIC_ADC_Init(NVIC_PR0,ENABLE);
}

// SPI用于学电板载的FLASH芯片而不是LCD(暂时没有用到)
// static void STC8x_SPI_Config(void)
// {
// 	SPIInit_Type SPI_InitStruct = {0};

// 	// 推挽输出。 CS / SDA / SCL / RST / DC
// 	//GPIO_MODE_OUT_PP(GPIO_P1, Pin_2 | Pin_3 | Pin_5 | Pin_0 | Pin_1);
	
// 	// 设置映射，把SPI口映射到映射3(P4口)，默认为映射1
// 	// P_SW1[3:2]  	 00	     01	     10	     11
// 	//       	   映射1	映射2	 映射3	 映射4
// 	//   CS(SS) 	P12		P22		P54		P35
// 	//   SDA(MOSI)	P13		P23		P40		P34
// 	//   XXX(MISO)	P14		P24		P41		P33
// 	//   SCL(SCLK)	P15		P25		P43		P32
// 	GPIO_SPI_SWPort(SW_Port3);
	
// 	SPI_InitStruct.Type = SPI_Type_Master;
// 	SPI_InitStruct.ClkSrc = SPI_SCLK_DIV_4;
// 	SPI_InitStruct.Mode = SPI_Mode_0; // Mode0: CPOL=0 / CPHA=0   Mode2: CPOL=1 / CPHA=0
// 	SPI_InitStruct.Tran = SPI_Tran_MSB;
// 	SPI_InitStruct.Run = ENABLE;
// 	SPI_Init(&SPI_InitStruct);
// }

/**
  * @name    STC8x_PWM_Config
  * @brief   MCU ADC initialization function
  * @param   None
  * @return  None
***/
static void STC8x_PWM_Config(void)
{
	GPIO_MODE_OUT_PP(GPIO_P0,Pin_5);
	GPIO_MODE_OUT_PP(GPIO_P0,Pin_6);
	GPIO_MODE_OUT_PP(GPIO_P0,Pin_7);

	// 3只LED的PWM初始化，直接点亮有点刺眼所以调暗了些
	PWMn_Port_Init(PWM_Port_0, PWM_SCLK_DIV_16, 1000);
	PWMn_Channel_Init(PWM_Port_0, PWM_Channel_5, PWM_Start_Low_Level, 0, 20, DISABLE);
	PWMn_Channel_Init(PWM_Port_0, PWM_Channel_6, PWM_Start_Low_Level, 0, 50, DISABLE);
	PWMn_Channel_Init(PWM_Port_0, PWM_Channel_7, PWM_Start_Low_Level, 0, 100, DISABLE);
	PWMn_run(PWM_Port_0);

	/* 蜂鸣器PWM初始化 */
	// 使用了P53口，周期随便填了一个，不用很严谨，听个声儿就行
	PWMn_Port_Init(PWM_Port_5, PWM_SCLK_DIV_16, 0x20A0);
	PWMn_Channel_Init(PWM_Port_5, PWM_Channel_3, PWM_Start_High_Level, 0x20A0 / 2, 0, ENABLE);
}



/*-----------------------------------------------------------------------
|                   END OF FLIE.  (C) COPYRIGHT zeweni                  |
-----------------------------------------------------------------------*/
