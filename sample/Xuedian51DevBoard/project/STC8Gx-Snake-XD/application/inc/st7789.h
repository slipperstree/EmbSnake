#ifndef __ST7789_H
#define __ST7789_H

#include "common.h"

sbit TFT_BL = P3^4;   // Һ��������ƿ�������
sbit LCD_CS = P5^0;   // Ƭѡ����
sbit LCD_RS = P5^1;   // 
sbit LCD_WR = P4^2; 
sbit LCD_RD = P4^4;

void LCD_WR_REG(unsigned int reg);
void LCD_SetArea(unsigned int stx,unsigned int sty,unsigned int endx,unsigned int endy);
void LCD_Init(void);
void LCD_Clear(unsigned int color);
void LCD_DrawPoint(unsigned int x, unsigned int y);
void LCD_DrawLine(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);
void LCD_DrawRectangle(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);
void LCD_Fill(unsigned int stx, unsigned int sty, unsigned int endx, unsigned int endy, unsigned int color);
void LcdWirteColorData(unsigned int color);
void LCD_DrawLine_Color(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int width, unsigned int color);
#endif



