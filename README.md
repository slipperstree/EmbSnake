# EmbSnake

#### 介绍
EmbSnake是一款贪食蛇游戏。主要面向嵌入式环境，模块化设计，分离了设备相关性。只需要针对特定的设备实现一些必要的函数就可以很快移植到你的平台上。根据需要可剪裁各种功能以适应各种MCU的可用储存空间（ROM）。

#### 文件结构
- embSnake - 贪食蛇的主模块，设备无关。
- gameFW - 一套比较完善的利用贪食蛇模块开发的一套游戏框架，包含了开始画面，游戏画面，Demo画面和游戏结束画面。框架本身与设备无关并将设备相关的函数以接口的形式独立出来放在embSnakeDevice.h中。可以在这套框架下根据实际设备实现embSnakeDevice.c中的各种接口函数即可完成游戏开发。当然这只是游戏的一种实现，你完全可以不用这套框架独立调用贪食蛇模块自己从头开发游戏。
- sample - 利用该模块实现的一些移植示例工程。

#### 移植示例1 - 学电51开发板
一个比较完整的实现，使用了gameFW框架。Keil工程，运行在学电51开发板上。[硬件链接](https://item.taobao.com/item.htm?spm=a230r.1.14.19.63df4924HYqoWQ&id=645481198623&ns=1&abbucket=1#detail)
[查看代码](/sample/Xuedian51DevBoard/)

![学电版](/sample/Xuedian51DevBoard/sample.jpg)

#### 移植示例2 - 任天堂 Game & Watch 35周年纪念掌机

同样用了gameFW框架，目标MCU是STM32H7B。
[查看代码](/sample/game-and-watch-snake/)

![Game&Watch](/sample/game-and-watch-snake/Pics/Home.jpg)
![Game&Watch](/sample/game-and-watch-snake/Pics/Game.jpg)
![Game&Watch](/sample/game-and-watch-snake/Pics/Demo.jpg)
![Game&Watch](/sample/game-and-watch-snake/Pics/GameOver.jpg)

#### 移植示例3 - GBA（稍后更新）

#### 模块的使用方法
1. 引入embSnake下的文件到你的工程。
2. 修改embSnake.h中的地图大小等配置。
3. 在你的c文件中引用embSnake.h
4. 在你的c文件中定义一个回调函数 void eventSnake();
5. 在你的main函数中调用初始化函数并将上面定义的回调函数名传递进去。 SNAKE_init(eventSnake);
6. 调用函数SNAKE_restart()开始游戏或重启游戏。
7. 开始游戏后可以调用模块提供的各种函数来更新贪食蛇内部状态比如向上移动 SNAKE_moveUp();
8. 模块会管理当前游戏状态并在发生特定事件的时候回调eventSnake函数来通知主程序做响应，比如更新画面等。目前提供了以下事件。
    - 重启游戏。无参数
    - 游戏结束。参数1：分数
    - 请求刷新指定位置。参数1：需要更新的地图x坐标，参数2：需要更新的地图y坐标
    - 吃到苹果。参数1：分数
9. 主程序需要在回调函数 eventSnake 中判断事件ID，根据不同事件以及附带的参数来响应不同动作。事件ID及参数是使用全局变量来传递的，如下
    - SnakeEventId - 事件ID
    - SnakeEventParam1 - 参数1
    - SnakeEventParam2 - 参数2
10. 注意，核心模块是设备无关的。所以不会做任何与具体显示相关的动作，需要你自己在回调事件中完成。具体可参考sample是如何做的。
#### 参与贡献

欢迎Fork本仓库并提交Pull Request

